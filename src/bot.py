import configparser
import logging
import os
import re
import sys

import telebot

"""
El bot se llama FutbolApustuBot, los demás nombres
que se me ocurrían estan cogidos
"""

from telebot.types import ReplyKeyboardMarkup, KeyboardButton

# Import modules
sys.path.append("./data")
sys.path.append("./neuroevolution")
from neuroevolution.prediction import predict_revillas

# Archivo de configuración
config = configparser.ConfigParser(interpolation=configparser.BasicInterpolation())
path = '/'.join((os.path.abspath(__file__)).split('/')[:-1])
config.read(os.path.join(path, 'data', 'settings.cfg'))

users = {}
emoji_triste = '☹️'
emoji_feliz = '😀'
admin = os.environ['DONCELES_ADMIN']
bot_name = 'FutbolApustuBot'
logging.basicConfig(level=logging.INFO, handlers=[logging.FileHandler("bot.log"), logging.StreamHandler()])


def registrar_usuario(message, bot):
    # Registrar el usuario
    if message.chat.id not in users.keys():
        users[message.chat.id] = message.from_user.first_name
        msg = f"Usuarios conectados [{len(users)}]: "
        for k, v in users.items():
            msg += "\n- " + str(k) + ': ' + v
        logging.info(msg)
        # Enviar al admin el numero de usuarios conectados
        bot.send_message(admin, msg)


def partidos_disponibles(chat_id, bot):
    disponibles = ""
    for root, dirs, files in os.walk(config['PATHS']['predict_dir']):
        for npz_file in files:
            r = re.match(r'predict-([a-zA-Z_]+)-([a-zA-Z_]+)', npz_file)
            if r:
                local_name = r.group(1).replace('_', ' ')
                visitante_name = r.group(2).replace('_', ' ')

                disponibles += f"\n> {local_name} - {visitante_name}"
    if disponibles == "":
        bot.send_message(chat_id, f"Aún no hay partidos disponibles. Comprueba la hora de inicio "
                                  f"del partido más cercano y vuelve a intentarlo 30 minutos "
                                  f"antes de que empiece. ")
    else:
        bot.send_message(chat_id, f"Partidos disponibles:{disponibles}")


def pedir_numres(chat_id, bot):
    """
    Pide el número de resultados a predecir
    """
    # https://github.com/eternnoir/pyTelegramBotAPI#reply-markup
    markup = ReplyKeyboardMarkup(row_width=2, one_time_keyboard=True)
    item2 = KeyboardButton('Con 2')
    item3 = KeyboardButton('Con 3')
    markup.add(item2)
    markup.add(item3)
    bot.send_message(chat_id, "¿Predecir con 2 o 3 resultados?", reply_markup=markup)


def relizar_prediccion(model_name, chat_id, bot):
    # Realizar la predicción revillas
    bot.send_message(chat_id, f"Comenzando la predicción, dame un momento")
    sumario = predict_revillas(model_name)

    if sumario:
        for prediction in sumario:
            bot.send_message(chat_id, f"{prediction[0]} - {prediction[1]}\n{prediction[2]}", parse_mode='markdown')


def main():
    # Crear el bot
    bot = telebot.TeleBot(os.environ['FUTBOLAPUSTU_TOKEN'])
    telebot.logger.setLevel(logging.WARNING)  # Outputs debug messages to console.

    @bot.message_handler(commands=['start', 'help'])
    def send_welcome(message):
        registrar_usuario(message, bot)
        bot.send_message(message.chat.id, "Este bot es un intento de facilitar la predicción de los partidos de LaLiga."
                                          " Estas predicciones las realiza un modelo de redes neuronales (IA).\n"
                                          "\nOpciones"
                                          "\n---------"
                                          "\n- `/availables`: Muestra los partidos disponibles."
                                          "\n- `/predict`: Predice los partidos disponibles en el momento."
                                          "\n- `/show`: Envía imágenes de las arquitecturas de los modelos aprendidos.",
                         parse_mode="markdown")

    @bot.message_handler(commands=['availables'])
    def handle_message(message):
        registrar_usuario(message, bot)

        # Comprueba en la carpeta de predicciones si existe algun partido listo para predecir
        partidos_disponibles(message.chat.id, bot)

    @bot.message_handler(commands=['predict'])
    def handle_message(message):
        registrar_usuario(message, bot)

        # Pide el número de resultados
        pedir_numres(message.chat.id, bot)

        # Comprueba en la carpeta de predicciones si existe algun partido listo para predecir
        partidos_disponibles(message.chat.id, bot)

    @bot.message_handler(regexp="Con (2|3)")
    def handle_message(message):
        registrar_usuario(message, bot)

        # Realiza la predicción
        numres = re.match(r"Con ([23])", message.text).group(1)
        model_name = config['PATHS']['model_h5'].format(num_res=numres)
        if os.path.exists(model_name):
            relizar_prediccion(model_name, message.chat.id, bot)
        else:
            bot.send_message(message.chat.id, f"El modelo para {numres} resultados no esta disponible",
                             parse_mode='markdown')

    @bot.message_handler(commands=['show'])
    def handle_message(message):
        """
        Envía una foto con la arquitectura del modelo.
        """
        registrar_usuario(message, bot)

        pngfiles = [f for f in os.listdir(config['PATHS']['prod']) if
                    os.path.isfile(os.path.join(config['PATHS']['prod'], f)) and f[-4:] == '.png']
        for png in pngfiles:
            numres = png[6]
            assert int(numres), "Parece que no se encuentra el número de resultados en el nombre del modelo"
            with open(os.path.join(config['PATHS']['prod'], png), 'rb') as photo:
                bot.send_photo(message.chat.id, photo, f"Arquitectura del modelo CNN para {numres} resultados")

    @bot.message_handler(func=lambda x: True)
    def handle_message(message):
        logging.info(f"No se entiende el mensaje de {message.chat.id} ({message.from_user.first_name}) "
                     f"\n\t\t--> {message.text}")
        bot.send_message(message.chat.id, f"No entiendo lo que me quieres decir con: '{message.text}'")

        # Enviar mensaje a Revi si alguien envía un mensaje que no conoce, ni que fuera Facebook
        if message.chat.id != int(admin):
            bot.send_message(admin, f"{message.chat.id} ({message.from_user.first_name}): {message.text}")

    msg_final = f"{bot_name} deja de trabajar por hoy, en breve volveré {emoji_feliz}"
    try:
        logging.info(f"[+] El bot {bot_name} esta funcionando...")
        bot.polling()
    except KeyboardInterrupt:
        msg_final = f"[!] {bot_name} ha dejado de actualizar los mensajes nuevos"
        print(msg_final)
        bot.stop_polling()
    except Exception:
        msg_final = f"[!] Parece que ha habido un error en matrix {emoji_triste}"
        print(msg_final)
        bot.stop_polling()
    finally:
        logging.info(f"{bot_name} deja de trabajar")
        for (id, user) in users.items():
            logging.info(f"Avisando del cierre a {id}: ({user})")
            bot.send_message(id, msg_final, disable_notification=True)

        bot.stop_bot()


if __name__ == '__main__':
    main()
