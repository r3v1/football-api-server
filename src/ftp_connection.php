<?php 
    /**
    * Conexión al servidor FTP para obtener las últimas predicciones
    *
    * References
    * ----------
    * ftp_connect: https://www.php.net/manual/en/function.ftp-connect.php
    * ftp_login: https://www.php.net/manual/en/function.ftp-login.php
    */

    $server = 'ftp.futbolpredictor.eus';
    $port   =  21;
    $user   = "futbolpredictor";
    $pwd    = "8dvhTK47O&8v";

    $conn = ftp_connect($server, $port);

    // Try to login
    if(@ftp_login($conn, $user, $pwd)) {
        // https://stackoverflow.com/a/21044601
        // Raspberry Pi path
        $path = "ftp://$user:$pwd@$server//summary.json";
        // Localhost path
        // $path = "ftp://$user:$pwd@$server/ftp/summary.json";

        if (file_exists($path)) {
            $handle = fopen($path, "rb");
            $output = stream_get_contents($handle);
            fclose($handle);
        }
        else $output = null;

        $data = json_decode($output);
    }
    else {
        // No hay conexión al servidor FTP
        $data = json_decode(null);
    }
?>