<?php
    require 'ftp_connection.php';
    include 'counting.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Predicciones</title>
    <link rel='shortcut icon' type='image/ico' href='/favicon_32.ico' />
    <link rel="stylesheet" type="text/css" href="/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-cyan.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="/scripts.js"></script>
</head>
<body>
<!-- Always shows a header, even in smaller screens. -->
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">Predicciones</span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation. We hide it in small screens. -->
            <nav class="mdl-navigation">
                <!--Compartir página -->
                <a class="mdl-navigation__link">
                    <button id="share-toast" class="mdl-button mdl-js-button mdl-button--icon" type="button"
                            onclick="copyUrl();">
                        <span id="share-button" class="material-icons">share</span>
                        <div class="mdl-tooltip mdl-tooltip--large" data-mdl-for="share-button">Copiar la url</div>
                    </button>
                    <div id="demo-toast-example" class="mdl-js-snackbar mdl-snackbar">
                        <div class="mdl-snackbar__text"></div>
                        <button class="mdl-snackbar__action" type="button"></button>
                    </div>
                    <script>
                        (function () {
                            'use strict';
                            var snackbarContainer = document.querySelector('#demo-toast-example');
                            var showToastButton = document.querySelector('#share-toast');
                            showToastButton.addEventListener('click', function () {
                                'use strict';
                                var data = {message: 'Url copiada'};
                                snackbarContainer.MaterialSnackbar.showSnackbar(data);
                            });
                        }());
                    </script>
                </a>
            </nav>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Predicciones</span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="/index.php">Principal</a>
        </nav>
        <!--About-->
        <nav class="mdl-navigation" style="bottom: 0; position: fixed">
            <a class="mdl-navigation__link" href="/about.html">About</a>
        </nav>
    </div>
    <main class="mdl-layout__content">
        <div class="page-content demo-layout-transparent">
            <div>
                <div class="card card-1 col-md-6 text-md-center" id="card1" style="margin-top: 20px">
                    <h3>Predicciones de fútbol</h3>
                    <div class="text">
                        Aquí se muestran las predicciones de los partidos de fútbol de 15 competiciones distintas.
                        Para más información, puedes hacer click <a href="/about.html" style="color: #3f51b5;">aquí</a>.

                        <br>
                        <br>

                        En cada panel se muestra un partido. El color azul del panel indicará que el partido está siendo jugado en estos instantes mientras que un color blanco indicará que el partido ya ha sido jugado.

                        <ul>
                            <li><b>Modelo</b>: Algoritmo utilizado para la predicción del partido.</li>
                            <li><b>Accuracy</b>: Indica el porcentaje de partidos acertados en la fase de entrenamiento.</li>
                            <li><b>Predicción</b>: Estimación del resultado final del partido.</li>
                            <li><b>Tipo</b>: Forma en la que se ha estimado el resultado. El tipo <i>1-X2</i> estimará los resultados de la forma gana "local o no". El tipo <i>1-X-2</i> lo estimará de la forma "gana local, hay empate o gana visitante".</li>
                        </ul>

                        <br>

                        <b><i>Disclaimer</i>
                            <br>
                        <br>Se puede ver como un sistema de ayuda a la decisión: en ningún momento se dice qué equipo será el vencedor. Queda en manos del usuario la estimación del resultado final. No me hago responsable del mal uso que se le pueda dar.</b>


                    </div>
                </div>

                <?php
                    if (isset($data) and $data->matches > 0) {
                        // Cycle through the array
                        foreach ($data->predicts as $idx => $match) {
                            /*Cambiar color del card según si el partido esta en juego o no */
                            // Calcular cuántos minutos han pasado desde el partido
                            $start = new Datetime($match->date, new DateTimeZone('Europe/Madrid'));
                            $now = new Datetime('now', new DateTimeZone('Europe/Madrid'));
                            $mins_elapsed = ($now->getTimestamp() - $start->getTimestamp()) / 60;

                            // Aplicar el cambio de color si han pasado más de 150 minutos
                            echo '<div class="card card-1 col-md-6 text-md-center ';
                            if ($mins_elapsed > 150) echo '"';
                            else echo 'reciente"';
                            echo ' " id="card1" style="margin-top: 20px">';

                            echo '<table class="table table-hover" id="tabla-prediccion">';
                            echo '  <tbody>';
                            echo '  <tr>';
                            echo '      <td><span class="bold">Local</span></td>';
                            echo "      <td>$match->homeTeam</td>";
                            echo '  </tr>';
                            echo '  <tr>';
                            echo '      <td><span class="bold">Visitante</span></td>';
                            echo "      <td>$match->awayTeam</td>";
                            echo '  </tr>';
                            echo '  <tr>';
                            echo '      <td><span class="bold">Jornada</span></td>';
                            echo "      <td>$match->jornada</td>";
                            echo '  </tr>';
                            echo '  <tr>';
                            echo '      <td><span class="bold">Fecha</span></td>';
                            echo "      <td>";
                            echo $start->format('Y-m-d H\:i');
                            echo "      </td>";
                            echo '  </tr>';

                            echo '<tr>';
                            echo '<td colspan="2">';
                            echo '<table class="table table-hover" id="tabla-resultados" style="margin-top: 20px">';

                            echo '  <tr>';
                            echo '  <th>Modelo</th>';
                            echo '  <th>Accuracy</th>';
                            echo '  <th colspan="10">Predicción</th>';
                            echo '  <th>Tipo</th>';
                            echo '  </tr>';
                                foreach ($match->result as $idx => $m) {
                                    echo '<tr><td colspan="14"><hr></td></tr>';
                                    foreach ($m as $key => $result) {
                                        echo '<tr>';
                                        echo '<td><span class="bold">';

                                        # Mostrar el nombre del modelo
                                        $accuracy = 100 * floatval($result->accuracy);
                                        $splited = explode(" ", $result->model);
                                        if (count($splited) > 1) {
                                            $initial = $splited[0];
                                            $last = implode(array_slice($splited, 1));
                                            echo "$initial[0]. $last";
                                        }
                                        else echo "$result->model";
                                        echo '</span></td>';

                                        # Mostrar accuracy del modelo
                                        echo '<td colspan="2">';
                                        echo "$accuracy%";
                                        echo '</td>';

                                        # Mostrar predicción del modelo
                                        echo '<td colspan="3">';
                                        if ($result->result == 0) echo "1";
                                        else {
                                            if ($idx == "2") echo "X-2";
                                            else {
                                                if ($result->result == 1) echo "X";
                                                else echo "2";
                                            }
                                        }
                                        echo '</td>';

                                        echo '<td colspan="4">';
                                        if ($result->result == 0) echo "Local";
                                        else {
                                            if ($idx == "2") echo "Emp/Vis";
                                            else {
                                                if ($result->result == 1) echo "Empate";
                                                else echo "Visitante";
                                            }
                                        }
                                        echo '</td>';

                                        echo '<td colspan="5">';
                                        if ($idx == 2) echo "1-X2";
                                        if ($idx == 3) echo "1-X-2";
                                        echo '</td>';
                                        echo '</tr>';
                                    }     

                                }
                                echo '<tr><td colspan="14"><hr></td></tr>';
                                
                                
                            echo '</table>';
                            echo '</td>';
                            echo '</tr>';

                            // Close the table
                            echo '  </tbody>';
                            echo "</table>";

                        echo '</div>';
                        }
                    }
                    else {
                        echo '<div class="card card-1 col-md-6 text-md-center" id="card2">';
                        echo '    Parece que no hay predicciones disponibles...';
                        echo '</div>';
                    }
                ?>

                <div class="card card-1 col-md-6 text-md-center" id="card-title" style="margin-top: 20px">
                <div>
                    <h4>David Revillas</h4>
                    <div><i>Para cualquier pregunta o sugerencia, puedes escribirme a: <a style="color: #3f51b5;" href="mailto:r3v1@pm.me">r3v1@pm.me</a></i></div>
                    23-V-2020
                </div>
            </div>
            </div>
        </div>
    </main>
</div>


</body>
</html>