function fallbackCopyTextToClipboard(text) {
    // https://stackoverflow.com/a/30810322
    var textArea = document.createElement("textarea");
    textArea.value = text;

    // Avoid scrolling to bottom
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        var successful = document.execCommand('copy');
        // var msg = successful ? 'successful' : 'unsuccessful';
        // console.log('Fallback: Copying text command was ' + msg);
    } catch (err) {
        console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
}

function copyUrl() {
    // https://stackoverflow.com/a/30810322
    var url = window.location.href;
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(url);
        return;
    }
    navigator.clipboard.writeText(url).then(function () {
        // console.log('Async: Copying to clipboard was successful!');
    }, function (err) {
        console.error('Async: Could not copy text: ', err);
    });
}

function resultsCols(results) {
    let cols = [];
    for (var i = 0; i < results.length; i++) {
        cols.push([
            results[i]['model']
        ]);
    }

    return cols;
}

function resultsRows(results) {
    let rows = [];
    for (let i = 0; i < results.length; i++) {
        let res = null;
        if (results[i]['result'] === 0) {
            res = "1";
        } else {
            if (results['num_res'] === 2) {
                res = "X-2";
            } else {
                if (results[i]['result'] === 1) {
                    res = "X";
                } else {
                    res = "2";
                }
            }
        }

        rows.push([res]);
    }

    return rows;
}

function explanationRows(results) {
    let rows = [];
    for (let i = 0; i < results.length; i++) {
        let res = null;
        if (results[i]['result'] === 0) {
            res = "Local";
        } else {
            if (results['num_res'] === 2) {
                res = "Empate - Visitante";
            } else {
                if (results[i]['result'] === 1) {
                    res = "Empate";
                } else {
                    res = "Visitante";
                }
            }
        }

        rows.push([res]);
    }

    return rows;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function toPDF(summary) {
    // Generación del pdf
    // https://github.com/simonbengtsson/jsPDF-AutoTable/blob/master/examples/examples.js
    // https://simonbengtsson.github.io/jsPDF-AutoTable/#colstyles

    // window.print();
    // return false;

    if (summary['predicts'].length > 0 && summary['predicts'].length === summary['matches']) {

        var doc = new jsPDF();

        let primeraFecha = summary['predicts'][0]['date'];
        let ultimaFecha = summary['predicts'][summary['predicts'].length - 1]['date'];

        // Título
        doc.setFontSize(16);
        doc.text(summary['matches'] + ' predicciones desde ' + primeraFecha + ' a ' + ultimaFecha, 14, 22);

        for (var j = 0; j < summary['predicts'].length; j++) {

            // Información del partido
            doc.autoTable({
                columns: [
                    // {dataKey: 'competition', header: 'ID Competición'},
                    {dataKey: 'jornada', header: 'Jornada'},
                    {dataKey: 'date', header: 'Fecha'},
                    {dataKey: 'homeTeam', header: 'Local'},
                    {dataKey: 'awayTeam', header: 'Visitante'},
                ],
                body: [summary['predicts'][j]],
                startY: 30 + j * 45,
                // tableWidth: 'wrap',
                styles: {
                    cellPadding: 3.5,
                    // fontSize: 12,
                    // textColor: 0,
                    cellWidth: 'auto',
                    halign: 'center'
                },
                headStyles: {fillColor: [49, 79, 131]}, // Dark Blue
            });

            // Predicción
            doc.autoTable({
                columns: resultsCols(summary['predicts'][j]['result']),
                body: [resultsRows(summary['predicts'][j]['result']), explanationRows(summary['predicts'][j]['result'])],
                startY: 50 + j * 45,
                // tableWidth: 'wrap',
                styles: {
                    fontSize: 8,
                    cellWidth: 'auto',
                    halign: 'center'
                },
                pageBreak: 'avoid',
            });
        }

        // Contacto
        let finalY = doc.previousAutoTable.finalY;
        doc.setFontSize(8);
        doc.setFontStyle('italic');
        doc.text('Contacto: r3v1@pm.me', 14, finalY + 15);
        doc.text('Revi @ 2020', 178, finalY + 15);
         
        dialog = showDownloadDialog();
        await sleep(2000);
        doc.output('save', 'prediccion.pdf');
        //doc.output('dataurlnewwindow');
    }
}

function showProgressBar() {
    // Muestra la barra de progreso 
    var bar = document.getElementById('progressBar');
    bar.style.visibility = 'visible';
}

async function showDownloadDialog() {
    let dialog = document.getElementById('download-dialog');
    //var showDialogButton = document.getElementById('download-button');
    dialog.showModal();

    await sleep(2500);

    //dialog.querySelector('.close').addEventListener('click', function() {
    dialog.close();
    //});
}

function showHelpDialog() {
    var dialog = document.getElementById('help-dialog');
    var showDialogButton = document.getElementById('help-button');
    dialog.showModal();

    dialog.querySelector('.close').addEventListener('click', function() {
      dialog.close();
    });
}