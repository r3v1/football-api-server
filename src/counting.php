<?php
	/* Conteo de los visitantes a la página */

	// https://stackoverflow.com/a/3003233
	$ipAddress = $_SERVER['REMOTE_ADDR'];
	if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	    $ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
	}

	$now = new Datetime('now', new DateTimeZone('Europe/Madrid'));
	$timestamp = $now->format("Y-m-d H:i:s");

	// Abrir el archivo y guardar la información
	$visitors = fopen("visitors.txt", "a");
	$info = "$ipAddress,$timestamp\n";
	fwrite($visitors, $info);
	fclose($visitors);
?>